import java.util.Scanner;


public class OtherOperations {

    public static SavingsAccount s = new SavingsAccount();
    public static CurrentAccount c = new CurrentAccount();
    public static int flag =0;
    public static void main() throws InterruptedException {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter account number and pin");
        int acc_no = scanner.nextInt();
        int pin = scanner.nextInt();

        DataClass dataClass = new DataClass();
        try{
            if(pin == dataClass.getPin(acc_no)){
                if(dataClass.getType(acc_no) == "Savings"){
                    s.setName(dataClass.getName(acc_no));
                    s.setNumber(acc_no);
                    s.setBalance(dataClass.getBalance(acc_no));
                }
                else{
                    c.setName(dataClass.getName(acc_no));
                    c.setNumber(acc_no);
                    c.setBalance(dataClass.getBalance(acc_no));
                }
                flag = 1;
            }
            else{
                System.out.println("Incorret pin or account number please check..");
            }

        }catch (Exception e){
            flag = 0;
            System.out.println("Incorret pin or account number please check..");
        }

        if(flag == 1){
            System.out.println("--------Welcome!!!--------");

            if(dataClass.getType(acc_no) == "Savings" ){
                while(true){

                    System.out.println("\n\n---- Chose any action: ----");
                    System.out.println("1. Deposit money");
                    System.out.println("2. Withdraw money");
                    System.out.println("3. Check balance");
                    System.out.println("4. Fund transfer");
                    System.out.println("5. Mini statement");
                    System.out.println("6. Quit");

                    int choise = scanner.nextInt();
                    if(choise == 1){
                        System.out.println("Enter the amount to be depositted..");
                        int amount = scanner.nextInt();
                        s.Deposit(amount);
                        s.setBalance(dataClass.getBalance(acc_no));
                        System.out.println("Amount depositted successfully");
                        dataClass.addTransaction(acc_no, "Depositted  ₹"+amount);
                        System.out.println("Your current balance is "+ s.getBalance());
                    }else
                    if(choise == 2){
                        System.out.println("Enter the amount to be withdrawn..");
                        int amount = scanner.nextInt();
                        if(s.Withdraw(amount)){
                            System.out.println("Amount withdrawn successfully");
                            dataClass.addTransaction(acc_no, "Withdrawn  ₹"+amount);
                            System.out.println("Your current balance is "+ s.getBalance());
                        }
                        s.setBalance(dataClass.getBalance(acc_no));
                    }else
                    if(choise == 3){
                        System.out.println("Your current balance is "+ s.getBalance());
                    }else
                    if (choise == 4){
                        System.out.println("Enter the account number to which you want to send money..");
                        int num = scanner.nextInt();
                        System.out.println("Enter the amount you want to transfer");
                        int amount = scanner.nextInt();
                        if(s.Fundtransfer(num,amount)){
                            dataClass.addTransaction(acc_no, "fundtransfer to  " + num + "  ₹"+amount);
                            dataClass.addTransaction(num, "fund from  " + acc_no + "  ₹"+amount);
                            System.out.println("Fund transfered successfully!!!");
                        }
                        else
                            System.out.println("low balance !");
                    }
                    else
                    if (choise == 5)
                        dataClass.miniStatement(acc_no);
                    else
                        break;
                }
            }
            else{
                while(true){

                    System.out.println("\n\n---- Chose any action: ----");
                    System.out.println("1. Deposit money");
                    System.out.println("2. Withdraw money");
                    System.out.println("3. Check balance");
                    System.out.println("4. Fund transfer");
                    System.out.println("5. Ministatement");
                    System.out.println("6. Quit");

                    int choise = scanner.nextInt();
                    if(choise == 1){
                        System.out.println("Enter the amount to be depositted..");
                        int amount = scanner.nextInt();
                        c.Deposit(amount);
                        c.setBalance(dataClass.getBalance(acc_no));
                        System.out.println("Amount depositted successfully");
                        dataClass.addTransaction(acc_no,"Depositted  ₹"+amount);
                        System.out.println("Your current balance is "+ c.getBalance());
                    }else
                    if(choise == 2){
                        System.out.println("Enter the amount to be withdrawn..");
                        int amount = scanner.nextInt();
                        if(c.Withdraw(amount)){
                            System.out.println("Amount withdrawn successfully");
                            System.out.println("Your current balance is "+ c.getBalance());
                            dataClass.addTransaction(acc_no, "Withdrawn  ₹" +amount);
                        }
                        c.setBalance(dataClass.getBalance(acc_no));
                    }else
                    if(choise == 3){
                        System.out.println("Your current balance is "+ c.getBalance());
                    }else
                    if (choise == 4){
                        System.out.println("Enter the account number to which you want to send money..");
                        int num = scanner.nextInt();
                        System.out.println("Enter the amount you want to transfer");
                        int amount = scanner.nextInt();
                        if(c.Fundtransfer(num,amount)){
                            dataClass.addTransaction(acc_no, "fundtransfer to  " + num + "  ₹"+amount);
                            dataClass.addTransaction(num, "fund from  " + acc_no + "  ₹"+amount);
                            System.out.println("Fund transfered successfully!!!");
                        }
                        else
                            System.out.println("Low balance !");
                    }else
                    if (choise == 5)
                        dataClass.miniStatement(acc_no);
                    else
                        break;
                }
            }

        }

    }
}
