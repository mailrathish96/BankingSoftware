import java.io.IOException;
import java.util.Scanner;

/**
 * Created by rathi-pt1424 on 2/3/2017.
 */
public class Home {

    public static void main(String[] args) throws IOException, InterruptedException {
        while (true){
            System.out.println("\n--------- WELCOME ---------");
            System.out.println("Please select an action..");
            System.out.println("1. Create an account");
            System.out.println("2. Do other operations");
            System.out.println("3. Quit");
            Scanner scanner = new Scanner(System.in);
            int action = scanner.nextInt();

            if(action == 1){
                CreateAccount.main();
            }
            else
            if(action == 2){
                OtherOperations.main();
            }
            else
                if(action == 3)
                    break;
            else{
                System.out.println("Please check your input...");
            }
        }
        System.out.println("____THANK YOU!____");


    }
}
