import java.util.logging.Logger;

/**
 * Created by rathi-pt1424 on 2/6/2017.
 */
public class CurrentAccount extends Account{

    Logger logger = Logger.getLogger(SavingsAccount.class.getName());


    @Override
    public boolean Withdraw(int amount) throws InterruptedException {
        DataClass dataClass = new DataClass();
        int currentbalance = this.balance;
        if( currentbalance >= amount ) {
            if (currentbalance - amount >= MIN_BAL)
                dataClass.setBalance(this.acc_no, dataClass.getBalance(this.acc_no) - amount);
            else {
                dataClass.setBalance(this.acc_no, dataClass.getBalance(this.acc_no) - amount);
                Thread.sleep(1000);
                logger.info("you balance now is less than minimum balance..");
            }
            return true;
        }
        else{
            logger.severe("you don't have enough balance..");
            return false;
        }
    }

    @Override
    public void Deposit(int amount) {
        DataClass dataClass = new DataClass();
        dataClass.setBalance(this.acc_no, dataClass.getBalance(acc_no)+amount);
    }
}
