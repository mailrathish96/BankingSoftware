import java.util.*;

/**
 * Created by rathi-pt1424 on 2/7/2017.
 */
public class DataClass {
    public static HashMap record1 = new HashMap();
    public static HashMap record2 = new HashMap();
    public static HashMap record3 = new HashMap();
    public static HashMap record4 = new HashMap();
    public static ArrayList arrayList = new ArrayList();

    public void setAccount(int number, String name, int pin, int balance, String type){
        record1.put(number, name);
        record2.put(number, pin);
        record3.put(number, balance);
        record4.put(number, type);
    }

    public String getName(int number){
        return record1.get(number).toString();
    }

    public int getPin(int number){
        return (int)record2.get(number);
    }

    public int getBalance(int number){
        return (int)record3.get(number);
    }

    public String getType(int number){
        return record4.get(number).toString();
    }

    public void setBalance(int number, int balance){
        record3.put(number, balance);
    }

    public void printall(){
        Iterator iterator = record3.entrySet().iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }

    public void addTransaction(int  number, String tranfer){
        arrayList.add(number + " " +tranfer);
    }

    public void miniStatement(int number){
        Iterator iterator = arrayList.iterator();
        while (iterator.hasNext()){
            String array[] = iterator.next().toString().split(" ");
            if( Integer.parseInt(array[0]) == number ){
                for(int i=1;i<array.length;i++)
                    System.out.print(array[i]+" ");
                System.out.println();
            }
        }
    }
}
