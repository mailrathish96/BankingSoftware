import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class CreateAccount {

    public static SavingsAccount s;
    public static ArrayList arrayList;
    public static int account_count = 1000;

    public static void main() throws IOException {
        arrayList = new ArrayList();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your name");
        String name = "";
        name = scanner.nextLine();
        System.out.println("1. Savings Account");
        System.out.println("2. Current Account");
        int choise = scanner.nextInt();

        if(choise == 1) {
            DataClass dataClass = new DataClass();
            int pin = generatePIN();
            account_count = account_count+1;
            dataClass.setAccount(account_count,name,pin,0,"Savings");
            System.out.println("your Account number is "+account_count);
            System.out.println("your PIN number is "+pin);
        }else
            if(choise == 2){
                DataClass dataClass = new DataClass();
                int pin = generatePIN();
                account_count = account_count+1;
                dataClass.setAccount(account_count,name,pin,0,"Current");
                System.out.println("your Account number is "+account_count);
                System.out.println("your PIN number is "+pin);
            }
        else
                System.out.println("please check your input..");

        System.out.println("______________CONGRATILATIONS_______________");
        System.out.println("--- your account created successfully!!! ---");
    }


    public static int generatePIN(){
        int randomPIN = (int)(Math.random()*9000)+1000;
        return randomPIN;
    }
}
