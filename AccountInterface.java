/**
 * Created by rathi-pt1424 on 2/7/2017.
 */
public interface AccountInterface {

    public static final int MIN_BAL = 1000;
    public static final int MAX_WITHDRAW_LIMIT = 5;
}
