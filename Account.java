import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;


public abstract class Account implements AccountInterface{

    public int balance = 0;
    public int acc_no = 0;
    public String acc_name = "";


    DataClass dataClass = new DataClass();

    public abstract boolean Withdraw(int amount) throws InterruptedException;

    public abstract void Deposit(int amount);

    public boolean Fundtransfer(int acc_no, int amount){
        if(this.balance>amount){
            this.balance = this.balance - amount;
            dataClass.setBalance(this.acc_no, dataClass.getBalance(this.acc_no)-amount);
            dataClass.setBalance(acc_no,dataClass.getBalance(acc_no)+amount);
            return true;
        }
        else
            return false;
    }

    public void setName(String name){
        this.acc_name = name;
    }

    public void setNumber(int acc_no){
        this.acc_no = acc_no;
    }

    public void setBalance(int balance){
        this.balance = balance;
    }

    public int getBalance(){
        return this.balance;
    }

    public int getNumber(){
        return this.acc_no;
    }

}
