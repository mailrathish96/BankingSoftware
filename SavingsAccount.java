import com.sun.xml.internal.ws.server.ServerRtException;

import java.util.logging.Logger;


public class SavingsAccount extends Account{

    Logger logger = Logger.getLogger(SavingsAccount.class.getName());

    @Override
    public boolean Withdraw(int amount) throws InterruptedException {
        DataClass dataClass = new DataClass();
        int currentbalance = this.balance;
        if( currentbalance >= amount ) {
            if (currentbalance - amount >= MIN_BAL){
                this.balance = this.balance - amount;
                dataClass.setBalance(this.acc_no, this.balance);}
            else {
                this.balance = this.balance - amount;
                dataClass.setBalance(this.acc_no, this.balance);
                Thread.sleep(1000);
                logger.info("you balance now is less than minimum balance..");
            }
            return true;
        }
        else{
            logger.severe("you don't have enough balance..");
            return false;
        }

    }

    @Override
    public void Deposit(int amount) {
        DataClass dataClass = new DataClass();
        dataClass.setBalance(this.acc_no, dataClass.getBalance(acc_no)+amount);
    }
}
